# SPDX-FileCopyrightText: 2021 Mario Fux <mario@unormal.org>
#￼ SPDX-License-Identifier: GPL-2.0-or-later

import sys

from PySide2.QtCore import Qt, Slot
from PySide2.QtGui import QPainter
from PySide2.QtWidgets import (QAction, QApplication, QComboBox, QHeaderView, QHBoxLayout, QLabel, QLineEdit,
                               QMainWindow, QPushButton, QTableWidget, QTableWidgetItem,
                               QVBoxLayout, QWidget)
from PySide2.QtCharts import QtCharts
from PySide2.QtWidgets import QApplication, QLabel


import json
import mysql.connector
from SPARQLWrapper import SPARQLWrapper, JSON




# Temporary workaround for SSL problem with dbpedia
# (Source: https://stackoverflow.com/questions/50236117/scraping-ssl-certificate-verify-failed-error-for-http-en-wikipedia-org)
import ssl
ssl._create_default_https_context = ssl._create_unverified_context




class BWidget(QWidget):
    def __init__(self):
        QWidget.__init__(self)
        self.items = 0

        # More data
        sparql = SPARQLWrapper("https://dbpedia.org/sparql")
        
        #query = "SELECT ?r ?label ?kingdom {?r rdf:type dbo:Species . ?r rdfs:label ?label . OPTIONAL {?r dbo:kingdom ?kingdom} . FILTER (LANG(?label) = 'en')} LIMIT 20"
        #query = "SELECT ?r ?label ?kingdom {?r rdf:type dbo:Species . ?r rdfs:label ?label . OPTIONAL {?r dbo:kingdom ?kingdom} } LIMIT 20"
        query = "SELECT DISTINCT ?r { ?r  rdfs:subClassOf <http://www.w3.org/2002/07/owl#Thing>} ORDER BY ?r"
        #query = "SELECT DISTINCT ?r { ?r rdfs:subClassOf <http://dbpedia.org/ontology/Species> }"


        sparql.setQuery(query);
        sparql.setReturnFormat(JSON)
        results = sparql.query().convert()
        
        #print(self._data)
        #print(results)
        
        #self._data = results

        #i = 0;

        #for result in results["results"]["bindings"]:
            ######################
            ## Some output of data
            ##print(str(i) + " " + result["label"]["value"])
            #print(str(i) + " " + result["r"]["value"])
            ##print(str(i) + " " + result["label"]["xml:lang"])
            ##self._data[result["p"]["value"]] = result["label"]["value"]
            #i = i + 1;
        
        
        #print(results)
        
        
        # Left side of the application
        self.breadCrumb = QLabel("Navigation: Thing ->")
        self.classesCombo = QComboBox()
        self.search = QLineEdit()
        self.table = QTableWidget()
        self.table.setColumnCount(2)
        self.table.setHorizontalHeaderLabels(["Resource", "Label"])
        self.table.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        self.subClassesCombo = QComboBox()
        
        self.left = QVBoxLayout()
        self.left.setMargin(10)
        self.left.addWidget(self.breadCrumb)
        self.left.addWidget(self.classesCombo)
        self.left.addWidget(self.search)
        self.left.addWidget(self.table)
        self.left.addWidget(self.subClassesCombo)
        
        # Populate the classesCombo
        for result in results["results"]["bindings"]:
            self.classesCombo.addItem(result["r"]["value"])

        # Chart
        self.chartView = QtCharts.QChartView()
        self.chartView.setRenderHint(QPainter.Antialiasing)

        # Right
        self.superClass = QComboBox()
        self.description = QLineEdit()
        self.price = QLineEdit()
        self.add = QPushButton("Add")
        self.clear = QPushButton("Clear")
        self.quit = QPushButton("Quit")
        self.plot = QPushButton("Plot")

        # Disabling 'Add' button
        self.add.setEnabled(False)

        self.right = QVBoxLayout()
        self.right.setMargin(10)
        self.right.addWidget(self.superClass)
        self.right.addWidget(QLabel("Description"))
        self.right.addWidget(self.description)
        self.right.addWidget(QLabel("Price"))
        self.right.addWidget(self.price)
        self.right.addWidget(self.add)
        self.right.addWidget(self.plot)
        self.right.addWidget(self.chartView)
        self.right.addWidget(self.clear)
        self.right.addWidget(self.quit)

        # QWidget Layout
        self.layout = QHBoxLayout()

        #self.tableView.setSizePolicy(size)
        self.layout.addLayout(self.left)
        self.layout.addLayout(self.right)

        # Set the layout to the QWidget
        self.setLayout(self.layout)

        # Signals and Slots
        self.classesCombo.activated.connect(self.fillTable)
        self.classesCombo.activated.connect(self.fillSubClassesCombo)
        self.subClassesCombo.activated.connect(self.fillClassesCombo)
        #self.add.clicked.connect(self.addElement)
        #self.quit.clicked.connect(self.quitEpplication)
        #self.plot.clicked.connect(self.plotData)
        #self.clear.clicked.connect(self.clearTable)
        #self.description.textChanged[str].connect(self.checkDisable)
        #self.price.textChanged[str].connect(self.checkDisable)

        ## Fill example data
        #self.fillTable()
        
    @Slot()
    def quit_application(self):
        QApplication.quit()

    @Slot()
    def fillTable(self, index):
        print(self.classesCombo.currentText())
        
        # Clear the table widget
        self.items = 0
        self.table.clearContents()
        self.table.setRowCount(0)
        
        # TODO: Add language label aka FILTER
        query = "SELECT DISTINCT ?r ?label {?r rdf:type <" + self.classesCombo.currentText() + "> . ?r rdfs:label ?label } ORDER BY ?r LIMIT 20"
        print("Query: " + query)

        sparql = SPARQLWrapper("https://dbpedia.org/sparql")
        sparql.setQuery(query);
        sparql.setReturnFormat(JSON)
        results = sparql.query().convert()

        for result in results["results"]["bindings"]:
            resourceItem = QTableWidgetItem(result["r"]["value"])
            labelItem = QTableWidgetItem(result["label"]["value"])
            self.table.insertRow(self.items)
            self.table.setItem(self.items, 0, resourceItem)
            self.table.setItem(self.items, 1, labelItem)
            self.items += 1
            print(result["r"]["value"])

        
        #print(results)
        
    @Slot()
    def fillSubClassesCombo(self, index):
        
        query = "SELECT DISTINCT ?r { ?r rdfs:subClassOf <" + self.classesCombo.currentText() + "> } ORDER BY ?r" 
        
        sparql = SPARQLWrapper("https://dbpedia.org/sparql")
        sparql.setQuery(query);
        sparql.setReturnFormat(JSON)
        results = sparql.query().convert()
        
        self.subClassesCombo.clear()
        
        for result in results["results"]["bindings"]:
            self.subClassesCombo.addItem(result["r"]["value"])
            
    @Slot()
    def fillClassesCombo(self, index):
        
        query = "SELECT DISTINCT ?r { ?r rdfs:subClassOf <" + self.classesCombo.currentText() + "> } ORDER BY ?r" 
        #print("Here: " + query)
        
        sparql = SPARQLWrapper("https://dbpedia.org/sparql")
        sparql.setQuery(query);
        sparql.setReturnFormat(JSON)
        results = sparql.query().convert()
        
        self.classesCombo.clear()
        
        for result in results["results"]["bindings"]:
            self.classesCombo.addItem(result["r"]["value"])
            
        self.fillTable(0)
        


class BMainWindow(QMainWindow):
    def __init__(self, widget):
        QMainWindow.__init__(self)
        self.setWindowTitle("Bastian - DBpedia lookup application")

        # Set up the menu
        self.menu = self.menuBar()
        self.fileMenu = self.menu.addMenu("File")

        saveAction = QAction("Save", self)
        saveAction.setShortcut("Ctrl+S")
        saveAction.triggered.connect(self.exitApp)

        exitAction = QAction("Exit", self)
        exitAction.setShortcut("Ctrl+Q")
        exitAction.triggered.connect(self.exitApp)

        self.fileMenu.addAction(saveAction)
        self.fileMenu.addAction(exitAction)
        self.setCentralWidget(widget)

    @Slot()
    def exitApp(self, checked):
        QApplication.quit()


if __name__ == "__main__":
    app = QApplication(sys.argv)
    widget = BWidget()
    # QMainWindow using QWidget as central widget
    window = BMainWindow(widget)
    window.resize(1024, 768)
    window.show()

    sys.exit(app.exec_())
